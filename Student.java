public class Student{
	
	private String sport;
	private int time;
	private int sectionNumber;
	private int amountLearnt;
	
	public Student(String sport, int time){
	this.sport = sport;
	this.time =  time;
	this.sectionNumber = 3;
	this.amountLearnt = 0;
	}
	
	public void study(){
		System.out.println("Student studied for " +time+ " minutes");
	}
	
	public void gymClass(){
		System.out.println("Student played " +sport+ " during gym class");
	}
	
	public void learn(int amountStudied){
		System.out.println("Before: " + amountStudied);
		if(amountStudied > 0){
		amountLearnt += amountStudied;
		}
		System.out.println("After: " + amountLearnt);
	}
	
	public String getSport(){
		return this.sport;
	}
	
	
	public int getTime(){
		return this.time;
	}
	
	
	public int getSectionNumber(){
		return this.sectionNumber;
	}
	
	public void setSectionNumber(int newSectionNumber){
		this.sectionNumber = newSectionNumber;
	}
	
	public int getAmountLearnt(){
		return this.amountLearnt;
	}
	
	public void setAmountLearnt(int newAmountLearnt){
		this.amountLearnt = newAmountLearnt;
	}
	
	
	
}